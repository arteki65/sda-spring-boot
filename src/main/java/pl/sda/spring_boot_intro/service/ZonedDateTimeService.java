package pl.sda.spring_boot_intro.service;

import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;

@Component
public class ZonedDateTimeService implements DateTimeService {

    @Override
    public String now() {
        return ZonedDateTime.now().toString();
    }
}
