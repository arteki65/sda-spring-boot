package pl.sda.spring_boot_intro.service.startup;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import pl.sda.spring_boot_intro.config.AppConfig;
import pl.sda.spring_boot_intro.dto.reminders.ReminderDto;
import pl.sda.spring_boot_intro.service.CommentsService;
import pl.sda.spring_boot_intro.service.reminders.RemindersService;

import java.time.LocalDateTime;

@Component
@Profile("!disaster")
public class InitialRemindersCreator implements CommandLineRunner {

    private final RemindersService remindersService;

    private final AppConfig appConfig;

    public InitialRemindersCreator(RemindersService remindersService, AppConfig appConfig, CommentsService commentsService) {
        System.out.println("InitialRemindersCreator " + remindersService);
        this.remindersService = remindersService;
        this.appConfig = appConfig;
    }

    @Override
    public void run(String... args) {
        System.out.println(
                "InitialRemindersCreator will create " + appConfig.getSizeOfInitialDataReminders() + " initial reminders...");
        for (int i = 0; i < appConfig.getSizeOfInitialDataReminders(); i++) {
            ReminderDto reminderDto = new ReminderDto("init reminder " + i, LocalDateTime.now().plusMinutes(i));
            remindersService.createReminder(reminderDto);
        }
        System.out.println(
                "InitialRemindersCreator created " + appConfig.getSizeOfInitialDataReminders() + " initial reminders!");
    }
}
