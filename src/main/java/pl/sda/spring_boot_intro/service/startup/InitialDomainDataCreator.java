package pl.sda.spring_boot_intro.service.startup;

import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pl.sda.spring_boot_intro.config.AppConfig;
import pl.sda.spring_boot_intro.domain.User;
import pl.sda.spring_boot_intro.dto.comments.CreateCommentDto;
import pl.sda.spring_boot_intro.dto.posts.CretePostDto;
import pl.sda.spring_boot_intro.repository.users.UsersRepository;
import pl.sda.spring_boot_intro.service.CommentsService;
import pl.sda.spring_boot_intro.service.posts.PostsService;

import java.time.LocalDateTime;
import java.util.UUID;

@Component
public class InitialDomainDataCreator implements CommandLineRunner {

    private final UsersRepository usersRepository;

    private final PostsService postsService;

    private final CommentsService commentsService;

    private final AppConfig appConfig;

    private final PasswordEncoder passwordEncoder;

    public InitialDomainDataCreator(UsersRepository usersRepository, PostsService postsService,
            CommentsService commentsService, AppConfig appConfig, PasswordEncoder passwordEncoder) {
        this.usersRepository = usersRepository;
        this.postsService = postsService;
        this.commentsService = commentsService;
        this.appConfig = appConfig;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) {
        createAdminUser();
        usersRepository.save(UsersRepository.TECHNICAL_USER);
        createPosts();
        createComments();
    }

    private void createAdminUser() {
        System.out.println("Create admin user...");
        User admin = new User(UUID.randomUUID().toString(), "admin", passwordEncoder.encode("admin"), "admin@admin.com",
                LocalDateTime.now(), "ADMIN;USER", false);
        usersRepository.save(admin);
        System.out.println("admin user created!");
    }

    private void createPosts() {
        System.out.println(
                "InitialPostsCreator will create " + appConfig.getSizeOfInitialDataPosts() + " initial posts...");
        for (int i = 0; i < appConfig.getSizeOfInitialDataPosts(); i++) {
            CretePostDto cretePostDto = new CretePostDto("init title " + i, "init content " + i);
            postsService.createPost(cretePostDto, UsersRepository.TECHNICAL_USER.getUsername());
        }
        System.out.println("InitialPostsCreator created " + appConfig.getSizeOfInitialDataPosts() + " initial posts!");
    }

    private void createComments() {
        System.out.println("InitialDomainDataCreator will create " + appConfig.getSizeOfInitialDataComments()
                + " initial comments for each post...");
        postsService.getAllPosts().forEach(post -> {
            for (int i = 0; i < appConfig.getSizeOfInitialDataComments(); i++) {
                commentsService.createComment(
                        new CreateCommentDto("comment " + i, LocalDateTime.now().minusMinutes(i * 10L), post.getId()));
            }
        });
        System.out.println("InitialDomainDataCreator created " + appConfig.getSizeOfInitialDataComments()
                + " initial comments for each post...");
    }
}
