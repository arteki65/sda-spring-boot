package pl.sda.spring_boot_intro.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Primary
public class LocalDateTimeService implements DateTimeService {

    @Override
    public String now() {
        return LocalDateTime.now().toString();
    }
}
