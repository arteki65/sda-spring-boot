package pl.sda.spring_boot_intro.service.users;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sda.spring_boot_intro.domain.User;
import pl.sda.spring_boot_intro.dto.users.CreateUserDto;
import pl.sda.spring_boot_intro.dto.users.UserDto;
import pl.sda.spring_boot_intro.repository.users.UsersRepository;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static pl.sda.spring_boot_intro.service.reminders.RemindersServiceImpl.parseOptionalDateTime;

@Service
public class UsersServiceImpl implements UsersService {

    private final UsersRepository repository;

    private final PasswordEncoder passwordEncoder;

    public UsersServiceImpl(UsersRepository repository, PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User createUser(CreateUserDto userDto) {
        User createdUser = new User(UUID.randomUUID().toString(), userDto.getUsername(),
                passwordEncoder.encode(userDto.getPassword()), userDto.getEmail(), LocalDateTime.now(), "USER", false);
        repository.save(createdUser);
        return createdUser;
    }

    @Override
    public Collection<UserDto> getAllUsers() {
        return repository.findAll().stream().map(UserDto::new).collect(Collectors.toList());
    }

    public Collection<User> filterReminders(String dateTime, String text) {
        Optional<LocalDateTime> optionalDateTime = parseOptionalDateTime(dateTime);
        Optional<String> optionalText = Optional.ofNullable(text);

        if (optionalDateTime.isPresent() && optionalText.isPresent()) {
            return repository.findAllByDateTimeOfRegistrationAndEmail(optionalDateTime.get(), optionalText.get());
        }

        return optionalDateTime.map(repository::findAllByDateTimeOfRegistration).orElseGet(() -> {
            if (optionalText.isPresent()) {
                return repository.findAllByEmail(optionalText.get());
            }
            return repository.findAll();
        });
    }

    @Override
    public Optional<User> updateUser(CreateUserDto updatedCreateUserDto, String id) {
        Optional<User> userToUpdate = findUser(id);
        if (userToUpdate.isEmpty()) {
            return userToUpdate;
        }

        //delete old user
        User foundUser = userToUpdate.get();

        //create updated user
        User updatedUser = new User(foundUser.getId(), updatedCreateUserDto.getUsername(),
                updatedCreateUserDto.getPassword(), updatedCreateUserDto.getEmail(),
                foundUser.getDateTimeOfRegistration(), foundUser.getRoles(), foundUser.isLocked());
        repository.save(updatedUser);
        return Optional.of(updatedUser);
    }

    @Override
    public Optional<User> deleteUser(String id) {
        Optional<User> foundUser = findUser(id);
        if (foundUser.isEmpty()) {
            return foundUser;
        }
        User userToDelete = foundUser.get();
        repository.delete(userToDelete);
        return Optional.of(userToDelete);
    }

    private Optional<User> findUser(String id) {
        return repository.findById(id).filter(user -> user.getId().equals(id));
    }
}
