package pl.sda.spring_boot_intro.service.users;

import pl.sda.spring_boot_intro.domain.User;
import pl.sda.spring_boot_intro.dto.users.CreateUserDto;
import pl.sda.spring_boot_intro.dto.users.UserDto;

import java.util.Collection;
import java.util.Optional;

/*
    zdefinować interfejs i zaimplementować,
    do serwisu ma zostać przeniesiona logika biznesowa z kontrolera
 */
public interface UsersService {

    User createUser(CreateUserDto userDto);

    Collection<UserDto> getAllUsers();

    Optional<User> updateUser(CreateUserDto updatedCreateUserDto, String id);

    Optional<User> deleteUser(String id);
}
