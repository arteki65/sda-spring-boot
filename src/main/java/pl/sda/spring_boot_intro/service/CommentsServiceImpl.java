package pl.sda.spring_boot_intro.service;

import org.springframework.stereotype.Service;
import pl.sda.spring_boot_intro.domain.Comment;
import pl.sda.spring_boot_intro.dto.comments.CommentDto;
import pl.sda.spring_boot_intro.dto.comments.CreateCommentDto;
import pl.sda.spring_boot_intro.repository.posts.PostsRepository;
import pl.sda.spring_boot_intro.repository.reminders.CommentRepository;
import pl.sda.spring_boot_intro.repository.users.UsersRepository;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CommentsServiceImpl implements CommentsService {

    private final CommentRepository commentRepository;

    private final PostsRepository postsRepository;

    public CommentsServiceImpl(CommentRepository commentRepository, PostsRepository postsRepository) {
        this.commentRepository = commentRepository;
        this.postsRepository = postsRepository;
    }

    @Override
    public CommentDto createComment(CreateCommentDto createCommentDto) {
        System.out.println("CommentsController.createController(" + createCommentDto + ")");
        Comment createdComment = new Comment(UUID.randomUUID().toString(), createCommentDto.getContent(),
                createCommentDto.getDateTime(), UsersRepository.TECHNICAL_USER,
                postsRepository.findById(createCommentDto.getPostId()).orElseThrow(IllegalStateException::new));
        commentRepository.save(createdComment);
        return new CommentDto(createdComment);
    }

    //Read
    @Override
    public Collection<CommentDto> getAllComments() {
        return commentRepository.findAll().stream()
                .map(comment -> new CommentDto(comment.getId(), comment.getContent(), comment.getDateTime(),
                        comment.getUser().getUsername())).collect(Collectors.toList());
    }

    @Override
    public Optional<CommentDto> getComment(String id) {
        return findComment(id).map(CommentDto::new);
    }

    // Update
    @Override
    public Optional<CommentDto> updateComment(CreateCommentDto updatedCommentDto, String id) {
        Optional<Comment> commentToUpdate = findComment(id);
        if (commentToUpdate.isEmpty()) {
            return commentToUpdate.map(CommentDto::new);
        }

        //delete old comment
        Comment foundComment = commentToUpdate.get();

        // create update comment
        Comment updatedComment = new Comment(foundComment.getId(), updatedCommentDto.getContent(),
                updatedCommentDto.getDateTime(), UsersRepository.TECHNICAL_USER, foundComment.getPost());
        commentRepository.save(updatedComment);
        return Optional.of(updatedComment).map(CommentDto::new);
    }

    // Delete
    @Override
    public Optional<CommentDto> deleteComment(String id) {
        Optional<Comment> foundComment = findComment(id);

        if (foundComment.isEmpty()) {
            return foundComment.map(CommentDto::new);
        }

        Comment commentToDelete = foundComment.get();
        commentRepository.delete(commentToDelete);

        return Optional.of(commentToDelete).map(CommentDto::new);
    }

    private Optional<Comment> findComment(String id) {
        return commentRepository.findById(id);
    }

}
