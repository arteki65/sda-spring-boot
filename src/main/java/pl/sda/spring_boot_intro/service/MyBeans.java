package pl.sda.spring_boot_intro.service;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.sda.spring_boot_intro.domain.Reminder;
import pl.sda.spring_boot_intro.service.reminders.ReminderExecutor;

import java.time.LocalDateTime;
import java.util.Collection;

@Configuration
public class MyBeans {

    @Bean
    public CommandLineRunner displayDateTimeCmdLineRunner(Collection<DateTimeService> dateTimeServices) {
        return args -> {
            dateTimeServices.forEach(
                    dts -> System.out.println("now in spring -> " + dts.now() + " with " + dts));

        };
    }

    @Bean
    public CommandLineRunner cmdLineRunner(ReminderExecutor re) {
        return (args) -> {
            Reminder reminder = new Reminder("id", "SPRING SDA!!!", LocalDateTime.now());
            re.addReminder(reminder);
            re.execute();
        };
    }
}
