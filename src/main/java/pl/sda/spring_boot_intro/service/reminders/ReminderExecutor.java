package pl.sda.spring_boot_intro.service.reminders;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import pl.sda.spring_boot_intro.domain.Reminder;
import pl.sda.spring_boot_intro.service.DateTimeService;
import pl.sda.spring_boot_intro.service.DisplayService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Component
public class ReminderExecutor {

    private final List<Reminder> reminders = new ArrayList<>();

    // IoC
    private final DateTimeService dateTimeService;

    // IoC
    private final DisplayService displayService;

    // DI
    public ReminderExecutor(@Qualifier("zonedDateTimeService") DateTimeService dateTimeService, DisplayService displayService) {
        this.dateTimeService = dateTimeService;
        this.displayService = displayService;
    }

    public void addReminder(Reminder reminder) {
        reminders.add(reminder);
    }

    // for tests
    Collection<Reminder> getReminders() {
        return Collections.unmodifiableList(reminders);
    }

    public void execute() {
        /*String now = dateTimeService.now();
        List<Reminder> remindersToExecute = reminders.stream().filter(r -> r.getDateTime().isBefore(Local))
                .collect(Collectors.toList());
        remindersToExecute.forEach(r -> displayService.display(r.toString()));
        reminders.removeAll(remindersToExecute);*/
        displayService.display("Reminder Executor with " + dateTimeService);
    }
}
