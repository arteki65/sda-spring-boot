package pl.sda.spring_boot_intro.service.reminders;

import pl.sda.spring_boot_intro.domain.Reminder;
import pl.sda.spring_boot_intro.dto.reminders.ReminderDto;

import java.util.Collection;
import java.util.Optional;

public interface RemindersService {

    Reminder createReminder(ReminderDto reminderDto);

    Collection<Reminder> getAllReminders();

    Optional<Reminder> getReminder(String id);

    Collection<Reminder> filterReminders(String dateTime, String text);

    Optional<Reminder> updateReminder(ReminderDto updatedReminderDto, String id);

    Optional<Reminder> patchReminder(String id, String dateTime, String text);

    Optional<Reminder> deleteReminder(String id);
}
