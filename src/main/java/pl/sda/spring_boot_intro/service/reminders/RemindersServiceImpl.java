package pl.sda.spring_boot_intro.service.reminders;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pl.sda.spring_boot_intro.domain.Reminder;
import pl.sda.spring_boot_intro.dto.reminders.ReminderDto;
import pl.sda.spring_boot_intro.repository.reminders.RemindersRepository;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@Service
@Profile("!disaster")
public class RemindersServiceImpl implements RemindersService {

    private final RemindersRepository repository;

    public RemindersServiceImpl(RemindersRepository repository) {
        this.repository = repository;
    }

    @Override
    public Reminder createReminder(ReminderDto reminderDto) {
        Reminder createdReminder = new Reminder(UUID.randomUUID().toString(), reminderDto.getText(),
                reminderDto.getDateTime());
        repository.save(createdReminder);
        return createdReminder;
    }

    @Override
    public Collection<Reminder> getAllReminders() {
        return repository.findAll();
    }

    @Override
    public Optional<Reminder> getReminder(String id) {
        return findReminder(id);
    }

    @Override
    public Collection<Reminder> filterReminders(String dateTime, String text) {
        Optional<LocalDateTime> optionalDateTime = parseOptionalDateTime(dateTime);
        Optional<String> optionalText = Optional.ofNullable(text);

        if (optionalDateTime.isPresent() && optionalText.isPresent()) {
            return repository.findAllByDateTimeAndText(optionalDateTime.get(), optionalText.get());
        }

        return optionalDateTime.map(repository::findAllByDateTime).orElseGet(() -> {
            if (optionalText.isPresent()) {
                return repository.findAllByText(optionalText.get());
            }
            return repository.findAll();
        });
    }

    @Override
    public Optional<Reminder> updateReminder(@Valid ReminderDto updatedReminderDto, String id) {
        Optional<Reminder> reminderToUpdate = findReminder(id);
        if (reminderToUpdate.isEmpty()) {
            return reminderToUpdate;
        }

        Reminder foundReminder = reminderToUpdate.get();

        // create updated reminder
        Reminder updatedReminder = new Reminder(foundReminder.getId(), updatedReminderDto.getText(),
                updatedReminderDto.getDateTime());
        repository.save(updatedReminder);
        return Optional.of(updatedReminder);
    }

    @Override
    public Optional<Reminder> patchReminder(String id, String dateTime, String text) {
        Optional<LocalDateTime> optionalDateTime = parseOptionalDateTime(dateTime);
        Optional<String> optionalText = Optional.ofNullable(text);

        Optional<Reminder> foundReminder = findReminder(id);
        if (foundReminder.isEmpty()) {
            return foundReminder;
        }

        Reminder reminderToPatch = foundReminder.get();
        Reminder patchedReminder = new Reminder(reminderToPatch.getId(), optionalText.orElse(reminderToPatch.getText()),
                optionalDateTime.orElse(reminderToPatch.getDateTime()));

        repository.save(patchedReminder);
        return Optional.of(patchedReminder);
    }

    @Override
    public Optional<Reminder> deleteReminder(String id) {
        Optional<Reminder> foundReminder = findReminder(id);

        if (foundReminder.isEmpty()) {
            return foundReminder;
        }

        Reminder reminderToDelete = foundReminder.get();
        repository.delete(reminderToDelete);
        return Optional.of(reminderToDelete);
    }

    private Optional<Reminder> findReminder(String id) {
        return repository.findById(id);
    }

    public static Optional<LocalDateTime> parseOptionalDateTime(String dateTime) {
        Optional<LocalDateTime> optionalDateTime;
        if (dateTime == null || dateTime.isEmpty() || dateTime.isBlank()) {
            optionalDateTime = Optional.empty();
        } else {
            optionalDateTime = Optional.of(LocalDateTime.parse(dateTime));
        }
        return optionalDateTime;
    }
}
