package pl.sda.spring_boot_intro.service;

import org.springframework.stereotype.Component;

@Component
public class CmdLineDisplayService implements DisplayService {

    @Override
    public void display(String text) {
        System.out.println(text);
    }
}
