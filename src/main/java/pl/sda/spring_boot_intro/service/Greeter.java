package pl.sda.spring_boot_intro.service;

/**
 *
 * wprowadzić zależność typu DisplayService (pole w klasie Greeter)
 * wstrzyknąć zależność poprzez konstruktor
 * poinstruować springa aby utworzył obiekt klasy Greeter (użyć odpowiedniej adnotacji)
 * zdefinować metodę greet() która wywoła metodę display, z zależności DisplayService, z argumentem "Greetings from spring in SDA!"
 *
 */

public class Greeter {

}
