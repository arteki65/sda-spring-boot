package pl.sda.spring_boot_intro.service;

@FunctionalInterface
public interface DateTimeService {

    String now();
}

