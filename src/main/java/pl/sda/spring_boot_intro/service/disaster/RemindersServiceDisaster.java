package pl.sda.spring_boot_intro.service.disaster;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pl.sda.spring_boot_intro.domain.Reminder;
import pl.sda.spring_boot_intro.dto.reminders.ReminderDto;
import pl.sda.spring_boot_intro.service.reminders.RemindersService;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;

@Service
@Profile("disaster")
public class RemindersServiceDisaster implements RemindersService {

    private final LocalDateTime start = LocalDateTime.now();

    private final LocalDateTime end = start.plusHours(1);

    @Override
    public Reminder createReminder(ReminderDto reminderDto) {
        throw new DisasterException(start, end);
    }

    @Override
    public Collection<Reminder> getAllReminders() {
        throw new DisasterException(start, end);
    }

    @Override
    public Optional<Reminder> getReminder(String id) {
        throw new DisasterException(start, end);
    }

    @Override
    public Collection<Reminder> filterReminders(String dateTime, String text) {
        throw new DisasterException(start, end);
    }

    @Override
    public Optional<Reminder> updateReminder(ReminderDto updatedReminderDto, String id) {
        throw new DisasterException(start, end);
    }

    @Override
    public Optional<Reminder> patchReminder(String id, String dateTime, String text) {
        throw new DisasterException(start, end);
    }

    @Override
    public Optional<Reminder> deleteReminder(String id) {
        throw new DisasterException(start, end);
    }
}
