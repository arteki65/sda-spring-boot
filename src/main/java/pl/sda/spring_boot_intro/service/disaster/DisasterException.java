package pl.sda.spring_boot_intro.service.disaster;

import java.time.LocalDateTime;

public class DisasterException extends RuntimeException {
    private final LocalDateTime start;
    private final LocalDateTime end;

    public DisasterException(LocalDateTime start, LocalDateTime end) {
        this.start = start;
        this.end = end;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public LocalDateTime getEnd() {
        return end;
    }
}
