package pl.sda.spring_boot_intro.service.disaster;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pl.sda.spring_boot_intro.dto.posts.CretePostDto;
import pl.sda.spring_boot_intro.dto.posts.PostDto;
import pl.sda.spring_boot_intro.service.posts.PostsService;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;

@Service
@Profile("disaster")
public class PostsServiceDisaster implements PostsService {

    private final LocalDateTime start = LocalDateTime.now();

    private final LocalDateTime end = start.plusHours(1);
    @Override
    public PostDto createPost(CretePostDto cretePostDto, String username) {
        throw new DisasterException(start, end);
    }

    @Override
    public Collection<PostDto> getAllPosts() {
        throw new DisasterException(start, end);
    }

    @Override
    public Optional<PostDto> getPost(String id) {
        throw new DisasterException(start, end);
    }

    @Override
    public Collection<PostDto> filterPosts(String title, String content, String dateTime, String userId) {
        throw new DisasterException(start, end);
    }

    @Override
    public Optional<PostDto> updatePost(CretePostDto updatedPostDto, String id) {
        throw new DisasterException(start, end);
    }

    @Override
    public Optional<PostDto> patchPost(String id, String title, String content, String dateTime, String userId) {
        throw new DisasterException(start, end);
    }

    @Override
    public Optional<PostDto> deletePost(String id) {
        throw new DisasterException(start, end);
    }
}
