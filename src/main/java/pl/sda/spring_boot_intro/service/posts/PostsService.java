package pl.sda.spring_boot_intro.service.posts;

import pl.sda.spring_boot_intro.dto.posts.CretePostDto;
import pl.sda.spring_boot_intro.dto.posts.PostDto;

import java.util.Collection;
import java.util.Optional;

public interface PostsService {

    PostDto createPost(CretePostDto postDto, String username);

    Collection<PostDto> getAllPosts();

    Optional<PostDto> getPost(String id);

    Collection<PostDto> filterPosts(String title, String content, String dateTime, String userId);

    Optional<PostDto> updatePost(CretePostDto updatedPostDto, String id);

    Optional<PostDto> patchPost(String id, String title, String content, String dateTime, String userId);

    Optional<PostDto> deletePost(String id);

}
