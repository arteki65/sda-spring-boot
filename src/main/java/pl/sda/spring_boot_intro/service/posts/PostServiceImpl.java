package pl.sda.spring_boot_intro.service.posts;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pl.sda.spring_boot_intro.domain.Post;
import pl.sda.spring_boot_intro.dto.posts.PostDto;
import pl.sda.spring_boot_intro.repository.posts.PostsRepository;
import pl.sda.spring_boot_intro.dto.posts.CretePostDto;
import pl.sda.spring_boot_intro.repository.users.UsersRepository;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Profile("!disaster")
public class PostServiceImpl implements PostsService {

    private final PostsRepository repository;

    private final UsersRepository usersRepository;

    public PostServiceImpl(PostsRepository repository, UsersRepository usersRepository) {
        this.repository = repository;
        this.usersRepository = usersRepository;
    }

    @Override
    public PostDto createPost(CretePostDto cretePostDto, String username) {
        Post createdPost = new Post(UUID.randomUUID().toString(), cretePostDto.getTitle(), cretePostDto.getContent(),
                LocalDateTime.now(), usersRepository.findByUsername(username).orElseThrow());
        repository.save(createdPost);
        return new PostDto(createdPost);
    }

    @Override
    public Collection<PostDto> getAllPosts() {
        return repository.findAll().stream().map(PostDto::new).collect(Collectors.toList());
    }

    @Override
    public Optional<PostDto> getPost(String id) {
        return findPost(id).map(PostDto::new);
    }

    @Override
    public Collection<PostDto> filterPosts(String title, String content, String dateTime, String userId) {
        LocalDateTime parsedDateTime = LocalDateTime.parse(dateTime);

        return repository.findAllByTitleAndContentAndDateTimeAndUser(title, content, parsedDateTime,
                UsersRepository.TECHNICAL_USER).stream().map(PostDto::new).collect(Collectors.toList());

    }

    @Override
    public Optional<PostDto> updatePost(CretePostDto updatedPostDto, String id) {
        Optional<Post> postToUpdate = findPost(id);
        if (postToUpdate.isEmpty()) {
            return Optional.empty();
        }

        Post foundPost = postToUpdate.get();

        // create updated post
        Post updatedPost = new Post(foundPost.getId(), updatedPostDto.getTitle(), updatedPostDto.getContent(),
                foundPost.getDateTime(), UsersRepository.TECHNICAL_USER);
        repository.save(updatedPost);
        return Optional.of(new PostDto(updatedPost));
    }

    @Override
    public Optional<PostDto> patchPost(String id, String title, String content, String dateTime, String userId) {
        Optional<String> optionalTitle = Optional.ofNullable(title);
        Optional<String> optionalContent = Optional.ofNullable(content);
        Optional<LocalDateTime> optionalDateTime = parseOptionalDateTime(dateTime);

        Optional<Post> foundPost = findPost(id);
        if (foundPost.isEmpty()) {
            return Optional.empty();
        }

        Post postToPatch = foundPost.get();
        Post patchedPost = new Post(postToPatch.getId(), optionalTitle.orElse(postToPatch.getTitle()),
                optionalContent.orElse(postToPatch.getContent()), optionalDateTime.orElse(postToPatch.getDateTime()),
                UsersRepository.TECHNICAL_USER);
        repository.save(patchedPost);
        return Optional.of(new PostDto(patchedPost));
    }

    @Override
    public Optional<PostDto> deletePost(String id) {
        Optional<Post> foundPost = findPost(id);
        if (foundPost.isEmpty()) {
            return Optional.empty();
        }

        Post postToDelete = foundPost.get();
        repository.delete(postToDelete);
        return Optional.of(new PostDto(postToDelete));
    }

    private Optional<Post> findPost(String id) {
        return repository.findById(id);
    }

    private static Optional<LocalDateTime> parseOptionalDateTime(String dateTime) {
        Optional<LocalDateTime> optionalDateTime;
        if (dateTime == null || dateTime.isEmpty() || dateTime.isBlank()) {
            optionalDateTime = Optional.empty();
        } else {
            optionalDateTime = Optional.of(LocalDateTime.parse(dateTime));
        }
        return optionalDateTime;
    }
}
