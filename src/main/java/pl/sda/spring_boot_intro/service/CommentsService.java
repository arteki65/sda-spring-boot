package pl.sda.spring_boot_intro.service;

import pl.sda.spring_boot_intro.dto.comments.CommentDto;
import pl.sda.spring_boot_intro.dto.comments.CreateCommentDto;

import java.util.Collection;
import java.util.Optional;

/*
    zdefinować interfejs i zaimplementować,
    do serwisu ma zostać przeniesiona logika biznesowa z kontrolera
 */
public interface CommentsService {
    CommentDto createComment(CreateCommentDto createCommentDto);

    Collection<CommentDto> getAllComments() ;

    Optional<CommentDto> getComment(String id);

   Optional<CommentDto> updateComment(CreateCommentDto updatedCommentDto, String id);

   Optional<CommentDto> deleteComment(String id);

}
