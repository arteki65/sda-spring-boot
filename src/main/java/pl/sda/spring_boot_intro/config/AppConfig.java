package pl.sda.spring_boot_intro.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.PositiveOrZero;

@Component
@ConfigurationProperties(prefix = "pl.sda.spring-boot-intro")
@Validated
public class AppConfig {

    @PositiveOrZero
    private int sizeOfInitialDataReminders;

    @PositiveOrZero
    private int sizeOfInitialDataPosts;

    @PositiveOrZero
    private int sizeOfInitialDataComments;

    public int getSizeOfInitialDataReminders() {
        return sizeOfInitialDataReminders;
    }

    public int getSizeOfInitialDataPosts() {
        return sizeOfInitialDataPosts;
    }

    public void setSizeOfInitialDataReminders(int sizeOfInitialDataReminders) {
        this.sizeOfInitialDataReminders = sizeOfInitialDataReminders;
    }

    public void setSizeOfInitialDataPosts(int sizeOfInitialDataPosts) {
        this.sizeOfInitialDataPosts = sizeOfInitialDataPosts;
    }

    public int getSizeOfInitialDataComments() {
        return sizeOfInitialDataComments;
    }

    public void setSizeOfInitialDataComments(int sizeOfInitialDataComments) {
        this.sizeOfInitialDataComments = sizeOfInitialDataComments;
    }
}
