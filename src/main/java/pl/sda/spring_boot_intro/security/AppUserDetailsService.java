package pl.sda.spring_boot_intro.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.sda.spring_boot_intro.repository.users.UsersRepository;

@Service
public class AppUserDetailsService implements UserDetailsService {

    private final UsersRepository usersRepository;

    public AppUserDetailsService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return usersRepository.findByUsername(username).map(UserDetailsAdapter::new)
                .orElseThrow(() -> new UsernameNotFoundException(username + " not found"));
    }
}
