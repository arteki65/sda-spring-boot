package pl.sda.spring_boot_intro.domain;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "app_user")
public class User {

    @Id
    private String id;
    private String username;
    private String password;
    private String email;
    private LocalDateTime dateTimeOfRegistration;

    private String roles; // ADMIN;USER ADMIN USER

    private boolean isLocked;

    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
    private Collection<Post> posts = new ArrayList<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
    private Collection<Comment> comments = new ArrayList<>();

    public User() {
        // needed by hibernate
    }

    public User(String id, String username, String password, String email, LocalDateTime dateTimeOfRegistration, String roles, boolean isLocked) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.dateTimeOfRegistration = dateTimeOfRegistration;
        this.roles = roles;
        this.isLocked = isLocked;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public LocalDateTime getDateTimeOfRegistration() {
        return dateTimeOfRegistration;
    }

    public Collection<Post> getPosts() {
        return posts;
    }

    public Collection<Comment> getComments() {
        return comments;
    }

    public String getRoles() {
        return roles;
    }

    public boolean isLocked() {
        return isLocked;
    }

    @Override
    public String toString() {
        return "User{" + "id='" + id + '\'' + ", username='" + username + '\'' + ", password='" + password + '\''
                + ", email='" + email + '\'' + ", dateTimeOfRegistration=" + dateTimeOfRegistration + ", roles='"
                + roles + '\'' + ", isLocked=" + isLocked + ", posts=" + posts + ", comments=" + comments + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof User))
            return false;

        User user = (User) o;

        if (isLocked != user.isLocked)
            return false;
        if (!id.equals(user.id))
            return false;
        if (!username.equals(user.username))
            return false;
        if (!password.equals(user.password))
            return false;
        if (!email.equals(user.email))
            return false;
        if (!dateTimeOfRegistration.equals(user.dateTimeOfRegistration))
            return false;
        if (!roles.equals(user.roles))
            return false;
        if (!posts.equals(user.posts))
            return false;
        return comments.equals(user.comments);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + username.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + email.hashCode();
        result = 31 * result + dateTimeOfRegistration.hashCode();
        result = 31 * result + roles.hashCode();
        result = 31 * result + (isLocked ? 1 : 0);
        result = 31 * result + posts.hashCode();
        result = 31 * result + comments.hashCode();
        return result;
    }
}
