package pl.sda.spring_boot_intro.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
public class Comment {

    @Id
    private String id;

    private String content;

    private LocalDateTime dateTime;

    @ManyToOne
    private User user;

    @ManyToOne
    private Post post;

    public Comment(String id, String content, LocalDateTime dateTime, User user, Post post) {
        this.id = id;
        this.content = content;
        this.dateTime = dateTime;
        this.user = user;
        this.post = post;
    }

    public Comment() {
        //Empty constructor for hibernate
    }

    public String getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public User getUser() {
        return user;
    }

    public Post getPost() {
        return post;
    }

    @Override
    public String toString() {
        return "Comment{" + "id='" + id + '\'' + ", content='" + content + '\'' + ", dateTime=" + dateTime + ", user="
                + user + ", post=" + post + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Comment))
            return false;

        Comment comment = (Comment) o;

        if (!id.equals(comment.id))
            return false;
        if (!content.equals(comment.content))
            return false;
        if (!dateTime.equals(comment.dateTime))
            return false;
        if (!user.equals(comment.user))
            return false;
        return post.equals(comment.post);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + content.hashCode();
        result = 31 * result + dateTime.hashCode();
        result = 31 * result + user.hashCode();
        result = 31 * result + post.hashCode();
        return result;
    }
}
