package pl.sda.spring_boot_intro.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class Reminder {

    @Id
    private String id;

    private String text;

    private LocalDateTime dateTime;

    public Reminder() {
        // used by hibernate
    }

    public Reminder(String id, String text, LocalDateTime dateTime) {
        this.id = id;
        this.text = text;
        this.dateTime = dateTime;
    }

    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    @Override
    public String toString() {
        return "Reminder{" + "id='" + id + '\'' + ", text='" + text + '\'' + ", dateTime=" + dateTime + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Reminder reminder = (Reminder) o;

        if (!id.equals(reminder.id))
            return false;
        if (!text.equals(reminder.text))
            return false;
        return dateTime.equals(reminder.dateTime);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + text.hashCode();
        result = 31 * result + dateTime.hashCode();
        return result;
    }
}
