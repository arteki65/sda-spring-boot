package pl.sda.spring_boot_intro.domain;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Post {

    @Id
    private String id;
    private String title;
    private String content;
    private LocalDateTime dateTime;

    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "post", cascade = CascadeType.REMOVE)
    private Collection<Comment> post;

    public Post(String id, String title, String content, LocalDateTime dateTime, User user) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.dateTime = dateTime;
        this.user = user;
    }

    public Post() {

    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public User getUser() {
        return user;
    }

    @Override
    public String toString() {
        return "Post{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", dateTime=" + dateTime +
                ", userId='" + user + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return Objects.equals(id, post.id) && Objects.equals(title, post.title) && Objects.equals(content, post.content) && Objects.equals(dateTime, post.dateTime) && Objects.equals(
                user, post.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, content, dateTime, user);
    }
}
