package pl.sda.spring_boot_intro.dto.posts;

import pl.sda.spring_boot_intro.domain.Post;

import java.time.LocalDateTime;

public class PostDto {

    private final String id;

    private final String title;

    private final String content;

    private final LocalDateTime dateTime;

    private final String author;

    // TODO: private final Collection<CommentDto> comment;

    public PostDto(String id, String title, String content, LocalDateTime dateTime, String author) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.dateTime = dateTime;
        this.author = author;
    }

    public PostDto(Post post) {
        this.id = post.getId();
        this.title = post.getTitle();
        this.content = post.getContent();
        this.dateTime = post.getDateTime();
        this.author = post.getUser().getUsername();
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        return "PostDto{" + "id='" + id + '\'' + ", title='" + title + '\'' + ", content='" + content + '\''
                + ", dateTime=" + dateTime + ", username='" + author + '\'' + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof PostDto))
            return false;

        PostDto postDto = (PostDto) o;

        if (!id.equals(postDto.id))
            return false;
        if (!title.equals(postDto.title))
            return false;
        if (!content.equals(postDto.content))
            return false;
        if (!dateTime.equals(postDto.dateTime))
            return false;
        return author.equals(postDto.author);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + content.hashCode();
        result = 31 * result + dateTime.hashCode();
        result = 31 * result + author.hashCode();
        return result;
    }
}
