package pl.sda.spring_boot_intro.dto.posts;

import javax.validation.constraints.NotBlank;
import java.util.Objects;

public class CretePostDto {

    @NotBlank
    private final String title;
    @NotBlank
    private final String content;

    public CretePostDto(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "PostDto{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CretePostDto postDto = (CretePostDto) o;
        return Objects.equals(title, postDto.title) && Objects.equals(content, postDto.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, content);
    }
}
