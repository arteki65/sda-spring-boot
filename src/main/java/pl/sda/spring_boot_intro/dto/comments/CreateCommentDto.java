package pl.sda.spring_boot_intro.dto.comments;

import java.time.LocalDateTime;

public class CreateCommentDto {

    private final String content;
    private final LocalDateTime dateTime;

    private final String postId;

    public CreateCommentDto(String content, LocalDateTime dateTime, String postId) {
        this.content = content;
        this.dateTime = dateTime;
        this.postId = postId;
    }

    public String getContent() {
        return content;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public String getPostId() {
        return postId;
    }
}
