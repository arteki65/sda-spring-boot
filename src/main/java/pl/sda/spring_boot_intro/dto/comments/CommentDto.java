package pl.sda.spring_boot_intro.dto.comments;

import pl.sda.spring_boot_intro.domain.Comment;

import java.time.LocalDateTime;

public class CommentDto {

    private final String id;

    private final String content;

    private final LocalDateTime dateTime;

    private final String author;

    public CommentDto(String id, String content, LocalDateTime dateTime, String author) {
        this.id = id;
        this.content = content;
        this.dateTime = dateTime;
        this.author = author;
    }

    public CommentDto(Comment comment) {
        this.id = comment.getId();
        this.content = comment.getContent();
        this.dateTime = comment.getDateTime();
        this.author = comment.getUser().getUsername();
    }

    public String getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        return "CommentDto{" + "id='" + id + '\'' + ", content='" + content + '\'' + ", dateTime=" + dateTime
                + ", author='" + author + '\'' + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof CommentDto))
            return false;

        CommentDto that = (CommentDto) o;

        if (!id.equals(that.id))
            return false;
        if (!content.equals(that.content))
            return false;
        if (!dateTime.equals(that.dateTime))
            return false;
        return author.equals(that.author);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + content.hashCode();
        result = 31 * result + dateTime.hashCode();
        result = 31 * result + author.hashCode();
        return result;
    }
}
