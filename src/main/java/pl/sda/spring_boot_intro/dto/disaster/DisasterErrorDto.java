package pl.sda.spring_boot_intro.dto.disaster;

/**
 * {
 * "msg": "Disaster test, don't worry.",
 * "timeSlot": {
 * "start": "2022-07-28T19:00",
 * "end": "2022-07-28T19:30"
 * }
 * }
 */
public class DisasterErrorDto {

    private final String msg = "Disaster test, don't worry.";

    private final TimeSlotDto timeSlot;

    public DisasterErrorDto(TimeSlotDto timeSlot) {
        this.timeSlot = timeSlot;
    }

    public TimeSlotDto getTimeSlot() {
        return timeSlot;
    }

    public String getMsg() {
        return msg;
    }
}
