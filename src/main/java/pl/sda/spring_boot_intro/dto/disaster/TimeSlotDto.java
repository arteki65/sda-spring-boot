package pl.sda.spring_boot_intro.dto.disaster;

import java.time.LocalDateTime;

public class TimeSlotDto {

    private final LocalDateTime start;

    private final LocalDateTime end;

    public TimeSlotDto(LocalDateTime start, LocalDateTime end) {
        this.start = start;
        this.end = end;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public LocalDateTime getEnd() {
        return end;
    }
}
