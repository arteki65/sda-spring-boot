package pl.sda.spring_boot_intro.dto.reminders;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

public class ReminderDto {

    @NotBlank
    private final String text;

    @Future
    private final LocalDateTime dateTime;

    public ReminderDto(String text, LocalDateTime dateTime) {
        this.text = text;
        this.dateTime = dateTime;
    }

    public String getText() {
        return text;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    @Override
    public String toString() {
        return "ReminderDto{" + "text='" + text + '\'' + ", dateTime=" + dateTime + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        ReminderDto that = (ReminderDto) o;

        if (!text.equals(that.text))
            return false;
        return dateTime.equals(that.dateTime);
    }

    @Override
    public int hashCode() {
        int result = text.hashCode();
        result = 31 * result + dateTime.hashCode();
        return result;
    }
}
