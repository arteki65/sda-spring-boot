package pl.sda.spring_boot_intro.dto.reminders;

public class ReminderTextCannotBeBlankErrorDto {

    private final String msg = "Reminder's text can't be blank. Please provide not blank text!";

    public String getMsg() {
        return msg;
    }
}
