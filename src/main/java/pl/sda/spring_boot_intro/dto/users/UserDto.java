package pl.sda.spring_boot_intro.dto.users;

import pl.sda.spring_boot_intro.domain.User;
import pl.sda.spring_boot_intro.dto.posts.PostDto;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.stream.Collectors;

public class UserDto {

    private final String id;

    private final String username;

    private final String password;

    private final String email;

    private final LocalDateTime dateTimeOfRegistration;

    private final Collection<PostDto> posts;

    public UserDto(String id, String username, String password, String email, LocalDateTime dateTimeOfRegistration,
            Collection<PostDto> posts) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.dateTimeOfRegistration = dateTimeOfRegistration;
        this.posts = posts;
    }

    public UserDto(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.email = user.getEmail();
        this.dateTimeOfRegistration = user.getDateTimeOfRegistration();
        this.posts = user.getPosts().stream().map(PostDto::new).collect(Collectors.toList());
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public LocalDateTime getDateTimeOfRegistration() {
        return dateTimeOfRegistration;
    }

    public Collection<PostDto> getPosts() {
        return posts;
    }

    @Override
    public String toString() {
        return "UserDto{" + "id='" + id + '\'' + ", username='" + username + '\'' + ", password='" + password + '\''
                + ", email='" + email + '\'' + ", dateTimeOfRegistration=" + dateTimeOfRegistration + ", posts=" + posts
                + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof UserDto))
            return false;

        UserDto userDto = (UserDto) o;

        if (!id.equals(userDto.id))
            return false;
        if (!username.equals(userDto.username))
            return false;
        if (!password.equals(userDto.password))
            return false;
        if (!email.equals(userDto.email))
            return false;
        if (!dateTimeOfRegistration.equals(userDto.dateTimeOfRegistration))
            return false;
        return posts.equals(userDto.posts);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + username.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + email.hashCode();
        result = 31 * result + dateTimeOfRegistration.hashCode();
        result = 31 * result + posts.hashCode();
        return result;
    }
}
