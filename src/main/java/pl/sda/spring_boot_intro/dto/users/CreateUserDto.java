package pl.sda.spring_boot_intro.dto.users;

import java.util.Objects;

public class CreateUserDto {

    private final String username;
    private final String password;
    private final String email;

    public CreateUserDto(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "username='" + username + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateUserDto userDto = (CreateUserDto) o;
        return username.equals(userDto.username) && password.equals(userDto.password) && email.equals(userDto.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password, email);
    }
}
