package pl.sda.spring_boot_intro.dto.validation;

public class ValidationErrorDto {

    private final String objectName;

    private final String fieldName;

    private final String msg;

    public ValidationErrorDto(String objectName, String fieldName, String msg) {
        this.objectName = objectName;
        this.fieldName = fieldName;
        this.msg = msg;
    }

    public String getObjectName() {
        return objectName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getMsg() {
        return msg;
    }
}
