package pl.sda.spring_boot_intro.repository.reminders;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import pl.sda.spring_boot_intro.domain.Comment;

import java.util.Optional;

public interface CommentRepository extends JpaRepository<Comment, String> {

}
