package pl.sda.spring_boot_intro.repository.reminders;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.spring_boot_intro.domain.Reminder;

import java.time.LocalDateTime;
import java.util.Collection;

public interface RemindersRepository extends JpaRepository<Reminder, String> {

    Collection<Reminder> findAllByDateTime(LocalDateTime dateTime);

    Collection<Reminder> findAllByText(String text);

    Collection<Reminder> findAllByDateTimeAndText(LocalDateTime dateTime, String text);
}
