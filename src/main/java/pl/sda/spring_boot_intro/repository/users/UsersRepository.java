package pl.sda.spring_boot_intro.repository.users;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.spring_boot_intro.domain.User;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public interface UsersRepository extends JpaRepository<User, String> {

    User TECHNICAL_USER = new User(UUID.randomUUID().toString(), "techUser", "techPassword", "techEmail",
            LocalDateTime.now(), "USER", false);

    Collection<User> findAllByDateTimeOfRegistration(LocalDateTime dateTime);

    Collection<User> findAllByEmail(String email);

    Collection<User> findAllByDateTimeOfRegistrationAndEmail(LocalDateTime dateTime, String email);

    Optional<User> findByUsername(String username);
}
