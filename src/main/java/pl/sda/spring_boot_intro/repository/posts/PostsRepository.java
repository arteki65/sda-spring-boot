package pl.sda.spring_boot_intro.repository.posts;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.spring_boot_intro.domain.Post;
import pl.sda.spring_boot_intro.domain.User;
import pl.sda.spring_boot_intro.dto.posts.PostDto;

import java.time.LocalDateTime;
import java.util.Collection;

public interface PostsRepository extends JpaRepository<Post, String> {

    Collection<Post> findAllByTitleAndContentAndDateTimeAndUser(String title, String content, LocalDateTime dateTime,
                                                                   User user);

}
