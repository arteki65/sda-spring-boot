package pl.sda.spring_boot_intro.controller.comment;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.sda.spring_boot_intro.dto.comments.CommentDto;
import pl.sda.spring_boot_intro.dto.comments.CreateCommentDto;
import pl.sda.spring_boot_intro.service.CommentsService;

import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping(path = "/comments")
@Secured({"ROLE_USER"})
public class CommentsController{

    private final CommentsService commentsService;

    public CommentsController(CommentsService commentsService) {
        this.commentsService = commentsService;
    }

    // Create
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CommentDto createComment(@RequestBody CreateCommentDto createCommentDto) {
        System.out.println("CommentsController.createController(" + createCommentDto + ")");
        return commentsService.createComment(createCommentDto);
    }

    //Read
    @GetMapping
    public Collection<CommentDto> getAllComments() {
        System.out.println("CommentController.getAllComments()");
        return commentsService.getAllComments();
    }

    // TODO: metoda do zwracania komentarzy z postu o przekazanym id, GET /byPost/{postId}, typ zwracany Collection<CommentDto>

    @GetMapping(path = "/{id}")
    public ResponseEntity<CommentDto> getComment(@PathVariable String id) {
        System.out.println("CommentsController.getComment(" + id + ")");

        return commentsService.getComment(id).map(comment -> new ResponseEntity<>(comment, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    // Update
    @PutMapping(path = "/{id}")
    public ResponseEntity<CommentDto> updateComment(@RequestBody CreateCommentDto updatedCommentDto, @PathVariable String id) {
        System.out.println("CommentsController.updateComment(" + updatedCommentDto + ", " + id + ")");

        Optional<CommentDto> updateComment = commentsService.updateComment(updatedCommentDto,id);
        if (updateComment.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(updateComment.get(), HttpStatus.OK);
    }

    // Delete
    @DeleteMapping("/{id}")
    public ResponseEntity<CommentDto> deleteComment(@PathVariable String id) {
        System.out.println("CommentsController.deleteComment(" + id + ")");
        Optional<CommentDto> deletedComment = commentsService.deleteComment(id);

        if (deletedComment.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(deletedComment.get(), HttpStatus.OK);
    }
}
