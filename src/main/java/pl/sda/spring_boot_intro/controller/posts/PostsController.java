package pl.sda.spring_boot_intro.controller.posts;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import pl.sda.spring_boot_intro.dto.posts.CretePostDto;
import pl.sda.spring_boot_intro.dto.posts.PostDto;
import pl.sda.spring_boot_intro.security.UserDetailsAdapter;
import pl.sda.spring_boot_intro.service.posts.PostsService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping(path = "/posts")
@Secured({"ROLE_USER"})
public class PostsController {

    private final PostsService postsService;

    public PostsController(PostsService postsService) {
        System.out.println("PostsController " + postsService);
        this.postsService = postsService;
    }

    // Create
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PostDto createPost(@RequestBody @Valid CretePostDto postDto, @AuthenticationPrincipal UserDetailsAdapter user) {
        System.out.println("PostsController.createPost(" + postDto + ")");
        return postsService.createPost(postDto, user.getUsername());
    }

    // Read
    @GetMapping
    public Collection<PostDto> getAllPosts() {
        System.out.println("PostsController.getAllPosts()");
        return postsService.getAllPosts();
    }

    // TODO: metoda do zwaracania postów zalogowanego użytkownika, GET /mine, typ zwracany Collection<PostDto>

    @GetMapping(path = "/{id}")
    public ResponseEntity<PostDto> getPost(@PathVariable String id) {
        System.out.println("PostsController.getPost(" + id + ")");
        return postsService.getPost(id).map(post -> new ResponseEntity<>(post, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping(path = "/filter")
    public Collection<PostDto> filterPosts(@RequestParam String title,
                                        @RequestParam String content,
                                        @RequestParam String dateTime,
                                        @RequestParam String userId) {
        return postsService.filterPosts(title, content, dateTime, userId);
    }

    // Update
    @PutMapping(path = "/{id}")
    public ResponseEntity<PostDto> updatePost(@RequestBody @Valid CretePostDto updatedPostDto,
                                           @PathVariable String id) {
        System.out.println("PostsController.updatePost(" + id + ")");

        Optional<PostDto> updatedPost = postsService.updatePost(updatedPostDto, id);
        if (updatedPost.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(updatedPost.get(), HttpStatus.OK);
    }

    @PatchMapping(path = "{id}")
    public ResponseEntity<PostDto> patchPost(@PathVariable String id,
                                          @RequestParam(required = false) String title,
                                          @RequestParam(required = false) String content,
                                          @RequestParam(required = false) String dateTime,
                                          @RequestParam(required = false) String userId) {
        System.out.println("PostsController.patchPost(" + id + ")");

        Optional<PostDto> patchedPost = postsService.patchPost(id, title, content, dateTime, userId);
        if (patchedPost.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(patchedPost.get(), HttpStatus.OK);
    }

    // Delete
    @DeleteMapping("{id}")
    public ResponseEntity<PostDto> deletePost(@PathVariable String id) {
        System.out.println("PostsController.deletePost(" + id + ")");

        Optional<PostDto> deletedPost = postsService.deletePost(id);
        if (deletedPost.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(deletedPost.get(), HttpStatus.OK);
    }
}
