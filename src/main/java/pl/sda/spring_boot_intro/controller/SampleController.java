package pl.sda.spring_boot_intro.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/test")
public class SampleController {

    @GetMapping
    public String test3() {
        System.out.println("test3() from SampleController");
        return "TEST3";
    }

    //    @RequestMapping(path = "/test", method = RequestMethod.GET)
    @GetMapping(path = "/{text}")
    public String test(@PathVariable String text) {
        System.out.println("test() from SampleController");
        return "Hello SDA Spring REST!!! text -> " + text;
    }

    @GetMapping(path = "/test2")
    public String test2(@RequestParam(required = false, defaultValue = "textDefaultValue") String text,
            @RequestParam String text2) {
        System.out.println("test2() from SampleController");
        return "test2 text -> " + text + "; text2 -> " + text2;
    }

    @PostMapping(path = "/post", consumes = MediaType.TEXT_PLAIN_VALUE)
    public String postTest(@RequestBody String body) {
        System.out.println("postTest() from SampleController");
        return "Post test! body -> " + body;
    }

    @DeleteMapping(path = "/delete")
    public void deleteTest() {
        System.out.println("deleteTest() from SampleController");
    }
}
