package pl.sda.spring_boot_intro.controller.exception;

import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.sda.spring_boot_intro.dto.validation.ValidationErrorDto;

import java.util.Collection;
import java.util.stream.Collectors;

@RestControllerAdvice
@Profile("!disaster")
public class ValidationExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Collection<ValidationErrorDto> handleValidationException(MethodArgumentNotValidException e) {
        return e.getFieldErrors().stream()
                .map(fe -> new ValidationErrorDto(fe.getObjectName(), fe.getField(), fe.getDefaultMessage()))
                .collect(Collectors.toList());
    }
}
