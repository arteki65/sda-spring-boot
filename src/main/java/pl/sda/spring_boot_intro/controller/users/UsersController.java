package pl.sda.spring_boot_intro.controller.users;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.sda.spring_boot_intro.domain.User;
import pl.sda.spring_boot_intro.dto.users.CreateUserDto;
import pl.sda.spring_boot_intro.dto.users.UserDto;
import pl.sda.spring_boot_intro.service.users.UsersService;

import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping(path = "/users")
public class UsersController {

    private final UsersService usersService;

    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    //Create
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserDto createUser(@RequestBody CreateUserDto userDto) {
        System.out.printf("Create user %s", userDto);
        return new UserDto(usersService.createUser(userDto));
    }

    //Read
    @Secured({"ROLE_ADMIN"})
    @GetMapping
    public Collection<UserDto> getAllUsers() {
        System.out.println("Get all users");
        return usersService.getAllUsers();
    }

    //Update
    @Secured({"ROLE_ADMIN"})
    @PutMapping("/{id}")
    public ResponseEntity<UserDto> updateUser(@RequestBody CreateUserDto updatedCreateUserDto, @PathVariable String id) {
        System.out.printf("Update user: %s with id: %s", updatedCreateUserDto, id);
        Optional<User> updatedUser = usersService.updateUser(updatedCreateUserDto, id);
        if (updatedUser.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new UserDto(updatedUser.get()), HttpStatus.OK);
    }

    // TODO: stworzyć metodę do banowania użytkowników, dostępna tylko dla admina (ROLE_ADMIN),
    // POST /lock/{id}, nie będzie request body, zwracany typ to void i status 200 OK


    //Delete
    @Secured({"ROLE_ADMIN"})
    @DeleteMapping("/{id}")
    public ResponseEntity<UserDto> deleteUser(@PathVariable String id) {
        System.out.printf("Delete id: %s", id);
        Optional<User> deletedUser = usersService.deleteUser(id);
        if (deletedUser.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new UserDto(deletedUser.get()), HttpStatus.OK);
    }
}
