package pl.sda.spring_boot_intro.controller.disaster;

import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.sda.spring_boot_intro.dto.disaster.DisasterErrorDto;
import pl.sda.spring_boot_intro.dto.disaster.TimeSlotDto;
import pl.sda.spring_boot_intro.service.disaster.DisasterException;

@RestControllerAdvice
@Profile("disaster")
public class DisasterExceptionHandler {

    @ExceptionHandler(DisasterException.class)
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    public DisasterErrorDto handleDisasterException(DisasterException de) {
        return new DisasterErrorDto(new TimeSlotDto(de.getStart(), de.getEnd()));
    }
}
