package pl.sda.spring_boot_intro.controller.reminders;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.spring_boot_intro.domain.Reminder;
import pl.sda.spring_boot_intro.dto.reminders.ReminderDto;
import pl.sda.spring_boot_intro.service.reminders.RemindersService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping(path = "/xml/reminders")
public class RemindersControllerXml {

    private final RemindersService remindersService;

    public RemindersControllerXml(RemindersService remindersService) {
        this.remindersService = remindersService;
    }

    // Create
    @PostMapping(consumes = MediaType.APPLICATION_XML_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Reminder createReminder(@RequestBody @Valid ReminderDto reminderDto) {
        System.out.println("RemindersController.createReminder(" + reminderDto + ")");
        return remindersService.createReminder(reminderDto);
    }

    // Read
    @GetMapping(produces = MediaType.APPLICATION_XML_VALUE)
    public Collection<Reminder> getAllReminders() {
        System.out.println("RemindersController.getAllReminders()");
        return remindersService.getAllReminders();
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Reminder> getReminder(@PathVariable String id) {
        System.out.println("RemindersController.getReminder(" + id + ")");
        return remindersService.getReminder(id).map(reminder -> new ResponseEntity<>(reminder, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping(path = "/filter", produces = MediaType.APPLICATION_XML_VALUE)
    public Collection<Reminder> filterReminders(@RequestParam(required = false) String dateTime,
            @RequestParam(required = false) String text) {
        return remindersService.filterReminders(dateTime, text);
    }

    // Update
    @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Reminder> updateReminder(@RequestBody @Valid ReminderDto updatedReminderDto,
            @PathVariable String id) {
        System.out.println("RemindersController.updateReminder(" + updatedReminderDto + ", " + id + ")");

        Optional<Reminder> updatedReminder = remindersService.updateReminder(updatedReminderDto, id);
        if (updatedReminder.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(updatedReminder.get(), HttpStatus.OK);
    }

    @PatchMapping(path = "/{id}", consumes = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Reminder> patchReminder(@PathVariable String id,
            @RequestParam(required = false) String dateTime, @RequestParam(required = false) String text) {
        System.out.println("RemindersController.patchReminder()");
        Optional<Reminder> patchedReminder = remindersService.patchReminder(id, dateTime, text);
        if (patchedReminder.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(patchedReminder.get(), HttpStatus.OK);
    }

    // Delete
    @DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Reminder> deleteReminder(@PathVariable String id) {
        System.out.println("RemindersController.deleteReminder(" + id + ")");
        Optional<Reminder> deletedReminder = remindersService.deleteReminder(id);

        if (deletedReminder.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(deletedReminder.get(), HttpStatus.OK);
    }
}
