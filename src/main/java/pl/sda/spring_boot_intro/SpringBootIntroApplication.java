package pl.sda.spring_boot_intro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootIntroApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootIntroApplication.class, args);
    }

    /*
    stowrzyć metodę cmdGreeter która zwraca obiekt typu CommandLineRunner (lambda lub klasa anonimowa)
    wstrzyknąć do metody cmdGreeter zależność typu Greeter poprzez argument (j. w.)
    wywołać metodę greet() z obiektu typu Greeter
    poinstruować springa (odpowiednia adnotacja) aby utowrzył umieścił w kontenerze IoC stworzony obiekt implementujący CommandLineRunner
     */
}
